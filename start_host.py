#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from dionysos.utils import IS_HARDWARE_AVAILABLE
from dionysos.hardware.connections import SensorConnection, DispenserConnection

# if not IS_HARDWARE_AVAILABLE:
#     raise EnvironmentError("This platform is not a Raspberry Pi !")

request = {
    "id": "565f1372-a4e1-4ce2-abe4-00af7f9cf5f5",
    "date": "2019-03-03T15:27:31.318569",
    "dispenser": "100f0000-a4e1-4ce2-abe4-00af7f9cf5f5",
    "volume": 160.0,
    "portions": [
        {"ratio": 0.25, "drink": 3},
        {"ratio": 0.75, "drink": 4}
    ]
}

d = DispenserConnection()
