#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pony.orm import db_session, select

from dionysos.utils import IS_CLIENT
from dionysos.ui.app import SmartDispenserApp

# For 3.5" raspberry pi 3 screen
from kivy.config import Config
Config.set('graphics', 'resizable', '0') #0 being off 1 being on as in true/false
Config.set('graphics', 'width', '480')
Config.set('graphics', 'height', '320')

with db_session:
    SmartDispenserApp(IS_CLIENT).run()
