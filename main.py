from mfrc522 import MFRC522 # https://github.com/danjperron/micropython-mfrc522
from utime import sleep
from machine import UART
import os

os.dupterm(None, 1)     # Désactivation du serial REPL.

uart = UART(0, baudrate=9600)



def uidToString(uid):
    mystring = ""
    for i in uid:
        mystring = "%02X" % i + mystring
    return mystring

rc522 = MFRC522(spi_id=0,sck=0,miso=4,mosi=2,cs=14,rst=5)

print("")
print("Placez une carte RFID pres du lecteur.")
print("")

while True:
    (stat, tag_type) = rc522.request(rc522.REQIDL)
    if stat == rc522.OK:
        (stat, uid) = rc522.SelectTagSN()
    if stat == rc522.OK:
        print("Carte detectee %s" % uidToString(uid))
	    uart.write(uidToString(uid))
        sleep(1) # delai pour éviter les lectures multiples
