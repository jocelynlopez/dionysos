# -*- coding: utf-8 -*-
import json
from pony.orm import commit, db_session

from dionysos.models import *

# Chargement des JSONs
with open("dionysos/db/drinks.json", "r") as df:
    drinks = json.load(df)
with open("dionysos/db/ingredients.json", "r") as ingf:
    ingredients = json.load(ingf)

with db_session:
    # User & Dispenser:
    #-----------------------------------------------------------------------------
    user = User(name="Jocelyn LOPEZ")
    dispenser = Dispenser(user=user)


    # Ingredients & IngredientType:
    #-----------------------------------------------------------------------------
    Ingredients = []
    kwargs = {
        'active': True,
        'current_vol': 0, 'max_vol': 1000,
        'current_level': 0, 'zero_level': 0, 'max_level': 0,
        'dispenser': dispenser
    }
    requested_drinks = ["coca-cola", "whisky", "rum", "gin", "orange juice", "tequila", "apple juice", "carbonated water"]
    IngredientTypeNames = {}
    IngredientsNames = {}
    for ingredient in ingredients:
        # IngredientType
        if ingredient['Type'] is None:
            ingredient_type = None
        elif ingredient['Type'] not in IngredientTypeNames:
            ingredient_type = IngredientType(name=ingredient['Type'])
            IngredientTypeNames[ingredient['Type']] = ingredient_type
        elif ingredient['Type'] in IngredientTypeNames:
            ingredient_type = IngredientTypeNames[ingredient['Type']]
            
        # Ingredient
        ingred = Ingredient(name=ingredient['Name'].lower(),
                            description=ingredient['Description'] or '',
                            name_FR=ingredient['NameFR'].lower(),
                            canDispense=ingredient['Dispense'],
                            thumb=ingredient['Thumb'],
                            thumb_small=ingredient['Thumb-Small'],
                            thumb_medium=ingredient['Thumb-Medium'],
                            ingredient_type=ingredient_type)
        IngredientsNames[ingredient['Name'].lower()] = ingred

        # Bottle
        if ingredient['Name'].lower() in requested_drinks:
            pos = requested_drinks.index(ingredient['Name'].lower()) + 1
            Bottle(position=pos, ingredient=ingred, **kwargs)

    # Drink & DrinkType & Portion & Glasses:
    #-----------------------------------------------------------------------------
    backups_drinks = []
    DrinkCatNames = {}
    GlassNames = {}
    AlcoolNames = {"alcoholic": True, "non alcoholic": False, None: None, "optional alcohol": None}
    for drink in drinks:
        # DrinkCategory
        if drink['Category'] is None:
            drink_cat = None
        elif drink['Category'] not in DrinkCatNames:
            drink_cat = DrinkCategory(name=drink['Category'])
            DrinkCatNames[drink['Category']] = drink_cat
        elif drink['Category'] in DrinkCatNames:
            drink_cat = DrinkCatNames[drink['Category']]

        # Glasses
        if drink['Glass'] is None:
            glass = None
        elif drink['Glass'] not in GlassNames:
            glass = Glass(name=drink['Glass'])
            GlassNames[drink['Glass']] = glass
        elif drink['Glass'] in GlassNames:
            glass = GlassNames[drink['Glass']]

        # Alcohol
        if drink['Alcoholic'] is None:
            alcool = None
        else:
            alcool = AlcoolNames[drink['Alcoholic'].lower()]

        # Portion
        portions = []
        for measure, ingred_name in zip(drink['Measures'], drink['Ingredients']):
            try:
                ingredient = IngredientsNames[ingred_name.lower()]
            except:
                backups_drinks.append(drink)
                continue

            if measure.__class__ in (int, float):
                volume, unit = measure, 'mL'
                continue
            if measure is None:
                continue
            r = measure.split(" ")
            if len(r) == 1 and measure.__class__ == str:
                try:
                    volume, unit = float(measure), ""
                except:
                    continue
            elif len(r) == 2 and measure.__class__ == str:
                try:
                    volume, unit = float(r[0]), r[1]
                except:
                    continue
            else:
                continue

            portions.append(Portion(ingredient=ingredient, volume=volume, unit=unit))

        if drink['NameFR'] is None: drink['NameFR'] = ""
        Drink(  name=drink['Name'].lower(),
                name_FR=drink['NameFR'].lower(),
                video=drink['Video'] or '',
                IBA=drink['IBA'] or '',
                alcoholic=alcool,
                instructions=drink['Instructions'] or '',
                instructions_FR=drink['InstructionsFR'] or '',
                thumb=drink['Thumb'],
                drink_category=drink_cat,
                glasses=glass,
                portions=portions)

commit()


with open("dionysos/db/drinks_to_handle.json", "r") as df:
    drinks = json.load(df)

with open("dionysos/db/drinks_to_handle.json", "w") as of:
    errors_drinks = drinks + backups_drinks
    json.dump(errors_drinks, of, indent=True)