#include <ArduinoJson.h>
#include "HX711.h"

// I/O pins configuration:
// -----------------------
int PIN_RX = 0;           // UART reading pin
int PIN_TX = 1;           // UART transmission pin
int PIN_CLK = A0;          // Clock pin of load cell
int PIN_CELL[] = {9, 10, 11, 12, 13};

// Settings:
// ---------
int NB_LC = 5;                // Number of load cell

// Setup basic objects:
// --------------------
HX711* CELLS = new HX711[NB_LC];

StaticJsonBuffer<100> jsonBuffer;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  // Setup load cells
  for(int i=0; i < NB_LC; i++){
    CELLS[i].begin(PIN_CELL[i], PIN_CLK);
  }
  Serial.println("{\"status\": \"ready\"}");  // println to write \n at the end for closure response
}

void loop() {
  if (Serial.available() > 0) {
    long StartTime = millis();
    
    // Read request
    String request_content = Serial.readStringUntil('\n');
    jsonBuffer.clear();
    JsonObject& request = jsonBuffer.parseObject(request_content);
    JsonObject& response = jsonBuffer.createObject();
    
    if (request.success()) {
      int cell = request["cell"];
      int sample = request["sample"];

      response["value"] = CELLS[cell-1].read_average(sample);
      response["cell"] = cell;
      response["sample"] = sample;
      response["status"] = "valid";
      response["elapsed_time"] = millis() - StartTime;
    } else {
      response["status"] = "error";
    }

    // Write response
    response.printTo(Serial);
    Serial.print("\n");   // write \n at the end for closure response
  }
}
