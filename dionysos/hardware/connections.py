# -*- coding: utf-8 -*-
import time
import serial
import json
from gpiozero import LED


class SensorConnection(serial.Serial):
    def __init__(self, *args, **kwargs):
        super(SensorConnection, self).__init__(*args, **kwargs)
        self._start_connection()

    def _start_connection(self, timeout=10):
        start = time.time()
        while True:
            r = self.readline().decode()
            try:
                if json.loads(r)["status"] == "ready":
                    break
                elif (time.time()-start) > timeout:
                    raise TimeoutError
            except json.decoder.JSONDecodeError:
                pass

    def send(self, request):
        # \n is mandatory to indicate end of transmission
        raw_request = json.dumps(request) + "\n"
        self.write(raw_request.encode('ascii'))
        raw_response = self.readline().decode('ascii')
        return json.loads(raw_response)

    def get_cell_value(self, cell):
        if cell > 10:
            raise SyntaxError
        request = {"cell": cell, "sample": 2}
        response = self.send(request)
        return response["value"]


class DispenserConnection:
    def __init__(self):
        self.sensor = SensorConnection(port="/dev/ttyACM0")
        self.VALVES = {
            0: LED(15, active_high=False),
            1: LED(12, active_high=False),
            2: LED(13, active_high=False),
            3: LED(14, active_high=False),
            4: LED(15, active_high=False),
            5: LED(15, active_high=False)
        }
        self.PUMP = LED(15, active_high=False)

    @staticmethod
    def isDispenseFinish(dispensed_vol, requested_vol):
        return dispensed_vol - requested_vol - 7

    def dispense(self, cmd, timeout=20):
        try:
            self.PUMP.on()
            self.VALVES[0].on()
            for bottle_id, volume, bottle_law in cmd:
                self.VALVES[bottle_id].on()
                dispensed_vol = 0
                start = time.time()
                while time.time()-start < timeout or self.isDispenseFinish(dispensed_vol, volume):
                    current_level = self.sensor.get_cell_value(bottle_id)
                    dispensed_vol = bottle_law(current_level)

        finally:
            self.VALVES[bottle_id].off()
            self.VALVES[0].off()
            self.PUMP.off()


if __name__ == "__main__":
    start = time.time()
    usb = SensorConnection(port="/dev/ttyACM0")
    print("Initialize connection in %.4f secondes" % (time.time()-start))

    for i in range(1, 10+1, 1):
        start = time.time()
        value = usb.get_cell_value(i)
        end = time.time() - start
        print("Cell %i value : %10i (%.4f secondes)" % (i, value, end))

    usb.close()
