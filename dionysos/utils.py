# -*- coding: utf-8 -*-

import io
import os


def is_raspberry_pi(raise_on_errors=False):
    if not os.path.isfile('/proc/cpuinfo'):
        return False
    try:
        with io.open('/proc/cpuinfo', 'r') as cpuinfo:
            found = False
            for line in cpuinfo:
                if line.startswith('Hardware'):
                    found = True
                    label, value = line.strip().split(':', 1)
                    value = value.strip()
                    if value not in ('BCM2708', 'BCM2709', 'BCM2835', 'BCM2836'):
                        if raise_on_errors:
                            raise ValueError('This system does not appear to be a Raspberry Pi.')
                        else:
                            return False
            if not found:
                if raise_on_errors:
                    raise ValueError('Unable to determine if this system is a Raspberry Pi.')
                else:
                    return False
    except:
        if raise_on_errors:
            raise ValueError('Unable to open `/proc/cpuinfo`.')
        else:
            return False

    return True

IS_CLIENT = not is_raspberry_pi()
IS_HARDWARE_AVAILABLE = is_raspberry_pi()
