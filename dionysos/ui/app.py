# -*- coding: utf-8 -*-
from collections import OrderedDict
from os.path import abspath, dirname, join, realpath
from time import time

from pony.orm import commit, db_session, select

from dionysos.ui.widgets import *
import dionysos.models as m


class SmartDispenserApp(App):
    time = NumericProperty(0)
    
    def __init__(self, isClient, *args, **kwargs):
        super(SmartDispenserApp, self).__init__(*args, **kwargs)
        self.kv_directory = join(self.directory, "kv_files")
        self.use_kivy_settings = False
        self.isClient = isClient
        self._update_db()

        # Load screen files
        Builder.load_file(join(self.kv_directory, "home.kv"))
        Builder.load_file(join(self.kv_directory, "drink.kv"))

    def _update_db(self):
        drinks = select(d for d in m.Drink).order_by(m.Drink.name)
        self.drinks = drinks

    def show_drinks(self):
        print("show_drinks()")

    def go_screen(self, name):
        self.root.ids.sm.current = name

    def update_list_drinks(self, drinks):
        idx = self.root.ids.sm.screen_names.index('list_drinks')
        list_drinks = self.root.ids.sm.screens[idx].ids.list_drinks
        for drink in drinks:
            list_drinks.add_widget(ItemDrink(drink))

    def build(self):
        # self.settings_cls = SettingsWithTabbedPanel
        self.title = 'Smart Dispenser by Jocelyn LOPEZ'
        self.root.ids.sm.switch_to(HomeScreen(name='home'))
        self.root.ids.sm.add_widget(ListDrinkScreen(name='list_drinks'))
        self.update_list_drinks(self.drinks[:30])

    def build_config(self, config):
        """
        Set the default values for the configs sections.
        """
        config.read(self.get_application_config())

    def build_settings(self, settings):
        """
        Add our custom section to the default configuration object.
        """
        _settings_panels = {
            "Bottles": join(self.directory, "settings", "bottles.json"),
            "Dispenser": join(self.directory, "settings", "dispenser.json"),
        }
        for name, file in _settings_panels.items():
            settings.add_json_panel(name, self.config, file)

