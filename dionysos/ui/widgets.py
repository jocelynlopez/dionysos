from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import (BooleanProperty, ListProperty, NumericProperty,
                             StringProperty)
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.carousel import Carousel
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import AsyncImage, Image
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.progressbar import ProgressBar
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.slider import Slider
from kivy.uix.widget import Widget
from kivy.config import ConfigParser
from kivy.uix.settings import SettingsWithTabbedPanel

import dionysos.models as m

class HomeScreen(Screen):
    pass

class ListDrinkScreen(Screen):
    pass

class ItemDrink(ButtonBehavior, GridLayout):
    def __init__(self, drink, **kwargs):
        super(ItemDrink, self).__init__(cols=1, **kwargs)
        self.drink = drink
        src = self.drink.thumb.replace("https://www.thecocktaildb.com/images/media/drink", "data/img/drinks")
        self.add_widget(Image(source=src, size_hint_x=1))
        self.add_widget(Label(text=self.drink.name, markup=True,
                              size_hint_x=1, size_hint_y=0.1, font_size='30sp'))

    def on_release(self):
        content = DispenseBox(self.drink)
        popup = Popup(content=content, title=self.drink.name,
                      auto_dismiss=False)
        content.bind_close(popup.dismiss)
        popup.open()

class DispenseBox(BoxLayout):
    def __init__(self, drink, **kwargs):
        super(DispenseBox, self).__init__(orientation='vertical', **kwargs)
        self.drink = drink

        self.label = Label()
        self.add_widget(self.label)

        volumeChoicesLayout = BoxLayout(orientation='horizontal')
        button5cL = Button(text='5cL')
        button5cL.bind(on_press=lambda x: self._update_volume(50))
        volumeChoicesLayout.add_widget(button5cL)
        button10cL = Button(text='10cL')
        button10cL.bind(on_press=lambda x: self._update_volume(100))
        volumeChoicesLayout.add_widget(button10cL)

        button20cL = Button(text='20cL')
        button20cL.bind(on_press=lambda x: self._update_volume(200))
        volumeChoicesLayout.add_widget(button20cL)

        button30cL = Button(text='30cL')
        button30cL.bind(on_press=lambda x: self._update_volume(300))
        volumeChoicesLayout.add_widget(button30cL)

        s = Slider(min=10, max=300, step=10,
                   value=150, orientation='vertical')
        s.bind(on_touch_move=lambda x, y: self._update_volume(x.value))
        volumeChoicesLayout.add_widget(s)
        self.add_widget(volumeChoicesLayout)

        self.pb = ProgressBar(value=0, max=200)
        self.add_widget(self.pb)

        buttonsLayout = BoxLayout(orientation='horizontal')
        dispenseButton = Button(text='Servir !')
        dispenseButton.bind(on_press=self.dispense)
        buttonsLayout.add_widget(dispenseButton)
        self.close_button = Button(text='Annuler')
        buttonsLayout.add_widget(self.close_button)
        self.add_widget(buttonsLayout)

        # self._update_volume(self.drink.volume)

    def dispense(self, *args):
        self.drink.dispense(self.volume)

    def _update_volume(self, volume):
        self.volume = volume
        self.label.text = "%i cL" % (volume/10.0)

    def bind_close(self, function):
        self.close_button.bind(on_press=function)
