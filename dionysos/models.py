#! -*- coding: utf-8 -*-
from datetime import datetime
from uuid import UUID
from pony.orm import Database, PrimaryKey, Required, Optional, Set, LongStr


db = Database()


class IngredientType(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    ingredients = Set('Ingredient')


class Ingredient(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    description = Optional(LongStr)
    name_FR = Optional(str)
    canDispense = Optional(bool)
    thumb = Optional(str)
    thumb_small = Optional(str)
    thumb_medium = Optional(str)
    ingredient_type = Optional(IngredientType)
    portions = Set('Portion')
    bottles = Set('Bottle')


class DrinkCategory(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    drinks = Set('Drink')


class Glass(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    drinks = Set('Drink')


class Drink(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    name_FR = Optional(str)
    video = Optional(str)
    IBA = Optional(str)
    alcoholic = Optional(bool)
    instructions = Optional(LongStr)
    instructions_FR = Optional(LongStr)
    thumb = Optional(str)
    date = Required(datetime, default=lambda: datetime.now())
    drink_category = Optional(DrinkCategory)
    glasses = Set(Glass)
    portions = Set('Portion')
    requests = Set('Request')

    def dispense(self, volume):
        print("dispense %s mL" % volume)

class Portion(db.Entity):
    id = PrimaryKey(int, auto=True)
    ingredient = Required(Ingredient)
    volume = Required(float)
    unit = Optional(str)
    drinks = Set(Drink)


class Request(db.Entity):
    id = PrimaryKey(int, auto=True)
    date = Required(datetime, default=lambda: datetime.now())
    volume = Required(float)
    drink = Required(Drink)
    dispenser = Required('Dispenser')


class Dispenser(db.Entity):
    id = PrimaryKey(UUID, auto=True)
    user = Required('User')
    bottles = Set('Bottle')
    requests = Set(Request)


class Bottle(db.Entity):
    id = PrimaryKey(int, auto=True)
    ingredient = Required(Ingredient)
    active = Required(bool)
    current_vol = Optional(float)
    max_vol = Optional(float)
    current_level = Optional(float)
    zero_level = Optional(float)
    max_level = Optional(float)
    dispenser = Required(Dispenser)
    position = Optional(int, size=8, unsigned=True)

    def update_current_vol(self, level):
        """
        Return volume associated with given level from sensor.
        """
        self.current_vol = self.max_vol * (level - self.zero_level) / (self.max_level - self.zero_level)
        return self.current_vol


class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    dispensers = Set(Dispenser)


db.bind(provider='sqlite', filename='dispenser.db', create_db=True)
db.generate_mapping(create_tables=True)
